# sysvinit (debconf) translation to Catalan.
# Copyright (C) 1997-2005
# This file is distributed under the same license as the sysvinit package.
#
#         Innocent De Marchi <tangram.peces@gmail.com>, 2011-2012.
#
msgid ""
msgstr ""
"Project-Id-Version: sysvinit 2.88dsf-13.1\n"
"Report-Msgid-Bugs-To: sysvinit@packages.debian.org\n"
"POT-Creation-Date: 2012-09-29 13:56-0400\n"
"PO-Revision-Date: 2012-06-12 18:14+0100\n"
"Last-Translator: Innocent De Marchi <tangram.peces@gmail.com>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Catalan\n"
"X-Poedit-Country: SPAIN\n"

#. Type: note
#. Description
#: ../sysv-rc.templates:1001
msgid "Unable to migrate to dependency-based boot system"
msgstr ""
"No ha estat possible migrar al sistema d'arrencada basat en dependències"

#. Type: note
#. Description
#: ../sysv-rc.templates:1001
msgid ""
"Problems in the boot system exist which are preventing migration to "
"dependency-based boot sequencing:"
msgstr ""
"Existeixen els següents problemes en el sistema d'arrencada, que impedeixen "
"la migració a la seqüència d'engegada basada en dependències:"

#. Type: note
#. Description
#: ../sysv-rc.templates:1001
msgid ""
"If the reported problem is a local modification, it needs to be fixed "
"manually.  These are typically due to obsolete conffiles being left after a "
"package has been removed, but not purged.  It is suggested that these are "
"removed by running:"
msgstr ""
"Si el problema comunicat és una modificació local, cal solucionar-ho "
"manualment. Aquests problemes solen ser a causa de fitxers de configuració "
"obsolets que no s'han eliminat després de la eliminació d'un paquet, sense "
"haver-ho purgat. Es suggereix que s'eliminin executant:"

#. Type: note
#. Description
#: ../sysv-rc.templates:1001
msgid "${SUGGESTION}"
msgstr "${SUGGESTION}"

#. Type: note
#. Description
#: ../sysv-rc.templates:1001
msgid ""
"Package installation can not continue until the above problems have been "
"fixed.  To reattempt the migration process after these problems have been "
"fixed, run \"dpkg --configure sysv-rc\"."
msgstr ""
"La instal·lació del paquet no pot continuar fins que no es solucionin els "
"problemes llistats. Per tornar a intentar el proces de migració, una vegada "
"solucionats els problemes, executeu «dpkg --configure sysv-rc»."

#~ msgid "Migrate legacy boot sequencing to dependency-based sequencing?"
#~ msgstr ""
#~ "¿Voleu migrar l'antiga seqüència d'arrencada a la seqüència d'engegada "
#~ "basada en dependències?"

#~ msgid ""
#~ "The boot system is prepared to migrate to dependency-based sequencing. "
#~ "This is an irreversible step, but one that is recommended: it allows the "
#~ "boot process to be optimized for speed and efficiency, and provides a "
#~ "more resilient framework for development."
#~ msgstr ""
#~ "El sistema d'arrencada està preparat per migrar a la seqüència d'engegada "
#~ "basada en dependències. Aquest canvi és irreversible, però es recomana "
#~ "fer-ho: permet optimitzar el procés d'engegada en velocitat i eficiència, "
#~ "i proporciona un entorn robust de desenvolupament."

#~ msgid ""
#~ "A full rationale is detailed in /usr/share/doc/sysv-rc/README.Debian. If "
#~ "you choose not to migrate now, you can do so later by running \"dpkg-"
#~ "reconfigure sysv-rc\"."
#~ msgstr ""
#~ "Al fitxer «/usr/share/doc/sysv-rc/README.Debian» hi ha l'explicació "
#~ "completa. Si decidiu no migrar ara, podreu fer-ho en el futur executant "
#~ "«dpkg-reconfigure sysv-rc»."

#~ msgid ""
#~ "If the reported problem is a local modification, it needs to be fixed "
#~ "manually. If it's a bug in the package, it should be reported to the BTS "
#~ "and fixed in the package. See http://wiki.debian.org/LSBInitScripts/"
#~ "DependencyBasedBoot for more information about how to fix the problems "
#~ "preventing migration."
#~ msgstr ""
#~ "Si el problema és una modificació local, cal endreçar-ho manualment. Si "
#~ "és un error del paquet, caldrà informar-ne mitjançant el BTS, perquè "
#~ "s'endreci al paquet. Per obtenir més informació sobre com resoldre els "
#~ "problemes que impedeixen la migració, llegiu «http://wiki.debian.org/"
#~ "LSBInitScripts/DependencyBasedBoot»."
